#include <stdio.h>
#define N 3
#define M 3
int mas[N][M];

int c(int *b) {
	int arr[256],x=0;
	for (int i = 0; i < 255; i++)
		arr[i] = 0;
	for (int i = 0; i < M; i++)
		arr[b[i]]++;
	for (int i = 0; i < 255; i++)
		if (arr[i]>x)
			x = arr[i];
	return x;
}
int f(int (*b)[M]) {
	int z=0;
	for (int i = 0; i < N; i++) {
		int x = 0;
		x=c(b[i]);
		if (x > z)
			z = x;
	}
	return z;
}

int main() {
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			scanf_s("%d", &mas[i][j]);
	printf("%d", f(mas));
	return 0;
}
