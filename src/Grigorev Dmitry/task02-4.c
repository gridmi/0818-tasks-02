#include <stdio.h>
#define re return
#define N 3
#define M 3
int mas[N][M];

int f(int (*b)[M]) {
	int z=0,a=0;
	for (int i = 0; i < M; i++) {
		int x = 0;
		for (int j = 0; j < N; j++)
			if (b[j][i]>x)
				x = b[j][i];
		if (x > z) {
			z = x;
			a = i;
		}
	}
	return a+1;
}

int main() {
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			scanf_s("%d", &mas[i][j]);
	printf("%d", f(mas));
	return 0;
}
